//Given the string 'ahb acb aeb aeeb adcb axeb'. Write a regular expression that matches the strings ahb, acb, aeb by pattern: letter 'a', any character, letter 'b'
const letterStr = 'ahb acb aeb aeeb adcb axeb';

let abc = (/a[a-z]b/g);
console.log(letterStr.match(abc))


//Given the string '2 + 3 223 2223'. Write a regex that finds line 2 + 3 without capturing the rest
const numStr = '2 + 3 223 2223';
let regex = numStr.match(/\b2\s*\+\s*3\b/);
if (regex) {
  console.log(regex[0]);
} else {
  console.log('Code not found');
}


//Get the day, month and year of the current date and output it to the console separately
const d = new Date();
console.log(d.toString())

let year = d.getFullYear();
let month = d.getMonth() + 1;
let date = d.getDate();

console.log(year);
console.log(month);
console.log(date);