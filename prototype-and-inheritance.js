//Create 2 objects: animal and cat, add move property to animal object, cat object must 
//inherit move property from object animal
class Animal {
    constructor (move) {
      this.move = move;
    }
    takeMove() {
      console.log(`The Animal says ${this.move}`)
    }
  }
  
  
  class Cat extends Animal {
    constructor(move) {
      super(move);
    }
    code() {
      console.log(`${this.move} is coding on JS`)
    }
  }
  
  
  let kitty = new Cat('I am moving');
  kitty.code();
  kitty.takeMove();