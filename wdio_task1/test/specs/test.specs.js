describe('Test suite', () => {
    it ('First test', async () => {
        await browser.url('https://pastebin.com/');
        const pageTitle = await browser.getTitle();

        console.log(pageTitle);
    });

    it('I Can Win', async () => {
        await headerBtn().click();
        await $("#postform-text").setValue("Hello from WebDriver");
        const selectBoxExpiration = $('[name = "PostForm[format]"]');
        selectBoxExpiration.click();
        const dropdownOptions = $$("//*[text()='10 Minutes']");
        dropdownOptions.waitForDisplayed();
        dropdownOptions.click();
        await $("#postform-name").setValue("helloweb");

        const submitButton = await $("//button[text() = 'Create New Paste']");
        await submitButton.scrollIntoView();
        await submitButton.click();
    })
})