// Create a car object, add a color property to it with the value equals 'black'
// Change the color property of the car object to 'green'
// Add the power property to the car object, which is a function and displays the engine power to the console

let car = {
    color: 'black',
    power: function getPower () {
       console.log('Engine power: 200')
    }
}
car.color = "green"; 


// Pears and apples are accepted to the warehouse, write a function that returns the result of adding the number of accepted pears and apples

function getWarehouse (acceptedApples, acceptedPears) {
  return acceptedApples + acceptedPears
}


// Your name is saved in the payment terminal, write a function to define the name in the terminal (if you entered your name, then hello + name, if not, then there is no such name)

function savedName (name) {
  if (name) {
    return "Hello, " + name
  } else {
    return "there is no such name"
  }
}


// Write a function for calculating the type of argument and type output to the console

function checkType (argument) {
  console.log(typeof argument)
}


//  Write a function that determines whether a number is prime or not

function isPrime(number) {
  if (number <= 1) {
    return false 
  } for (let i = 2; i < number; i++) {
    if (number % i === 0) {
      return false
    }
  }
  return true
} 