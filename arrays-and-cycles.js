//Given an array consisting of movie names, iterate over the array with the output of the names of each movie to the console
let arrayOfMovies = ['The Godfather', 'Citizen Kane','La Dolce Vita', 'Seven Samurai', 'Singing in the rain', 'There wiil be the blood'];
arrayOfMovies.forEach(movie => {
console.log(movie);
});


//Given an array of car manufacturers, convert the array to a string and back to an array
let carManufactures = ['Audi', 'Toyota', 'Citroen', 'Renault', 'Hyundai'];
let carString = carManufactures.toString();
let carArray = carString.split(',');
console.log(carString);
console.log(carArray);


//Given an array of the names of your friends, add the words hello to each element of the array
let friendsName = ['Alice', 'John', 'James', 'Mary', 'Tyler'];
let addGreeting = friendsName.map (name => 'Hello, ' + name)
console.log(addGreeting)


//Convert numeric array to Boolean
let arrNumber = [0, 4, 5, 7, 11, 20];
let booleanArr = arrNumber.map(num => Boolean(num));
console.log(booleanArr);


//Sort the array [1,6,7,8,3,4,5,6] in descending order
let arr1 = [1,6,7,8,3,4,5,6];
arr1.sort();
arr1.reverse();


//Filter array [1,6,7,8,3,4,5,6] by value> 3
let arr2 = [1,6,7,8,3,4,5,6];
let result = arr2.filter((value) => value > 3);
console.log(result);


//Write a function that takes two parameters - an array and a number and outputs the index of an array element equal to a number
function indexOfArray (arr, num) {
  return arr.indexOf(num);
}

let arr = [1,6,7,8,3,4,5,11];
let num = 11;
console.log(indexOfArray(arr, num));


//Implement a loop that will print the number 'a' until it is less than 10
let a = 0;
  while (a < 10) {
    console.log(a);
    a++
  }


//Implement a loop that prints odd numbers to the console
let maxNumber = 15; 
for (let number = 1; number <= maxNumber; number += 2) {
    console.log(number);
}


//Implement a loop that prints prime numbers to the console
function isPrime(number) {
  if (number <= 1) {
    return false 
  } for (let i = 2; i < number; i++) {
    if (number % i === 0) {
      return false
    }
  }
  return true
} 

function printPrime (start, end) {
  for (let number = start; number <= end; number++) {
        if (isPrime(number)) {
            console.log(number);
        }
    }
}
console.log(printPrime(0, 40))
