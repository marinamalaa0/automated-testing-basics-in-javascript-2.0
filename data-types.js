let stringValue = 'Hello, ';
let booleanValue = true;
let result = stringValue + booleanValue;
console.log(result) // srting + boolean

let stringValue1 = '10';
let numberValue = 5;
let result1 = Number(stringValue1) + numberValue;
console.log(result1) //string + number

let numberValue1 = 5;
let booleanValue1 = true;
let result2 = numberValue1 + booleanValue1;
console.log(result2) //number + boolean



let stringVal = "Hello!";
let booleanVal = true;
let res = stringVal.repeat(booleanVal);
console.log(res); //string * boolean

let stringVal1 = "Hello! ";
let numberVal = 7;
let res1 = stringVal1.repeat(numberVal);
console.log(res1); //string * number

let numberVal1 = 7;
let booleanVal1 = true;
let res2 = numberVal1 * booleanVal1;
console.log(res2); //number * boolean



let numberValue2 = 10;
let booleanValue2 = true;
let result3 = numberValue2 / booleanValue2;
console.log(result3); // number / boolean

let stringValue2 = '15';
let numberValue3 = 3;
let result4 = Number(stringValue2) / numberValue3;
console.log(result4); // string / number

let numberValue4 = 5;
let booleanValue4 = true;
let result5 = numberValue4/booleanValue4;
console.log(result5); //number / boolean


let result6 = numberValue4.toString();
console.log(result6); // number to string

let result7 = parseInt(stringValue2);
console.log(result7); //string to number

let result8 = Number(booleanValue4);
console.log(result8); //boolean to number

let result9 = booleanValue4.toString();
console.log(result9); //boolean to string
