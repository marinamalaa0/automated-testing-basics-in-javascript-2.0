describe('Bring It Out', () => {
    it ('Open Page', async () => {
        await browser.url('https://pastebin.com/');
        const pageTitle = await browser.getTitle();

        console.log(pageTitle);
    });

    it('Create a New Paste', async () => {
        await $('.header__btn').click();
        const codeTextArea = $("#postform-text");
        codeTextArea.setValue('git config --global user.name "New Sheriff in Town"\ngit reset $ (git commit-tree HEAD ^ {tree} -m "Legacy code")\ngit push origin master --force\n');
        
        const selectBoxExpiration = $('#select2-postform-expiration-container');
        selectBoxExpiration.click();
        const dropdownOptions = $("//*[text()='10 Minutes']");
        dropdownOptions[2].waitForDisplayed();
        dropdownOptions[2].click();

        const selectBox = $('[name = "PostForm[format]"]');
        selectBox.click();
        const selectBoxOption = $('//*[text()="Bash"]');
        selectBoxOption.waitForDisplayed();
        selectBoxOption.click();
        
        const pasteTitle = await $('#postform-name')
        pasteTitle.setValue("how to gain dominance among developers");

        const submitButton = await $("//button[text() = 'Create New Paste']");
        await submitButton.scrollIntoView();
        await submitButton.click();
    })

    it('Browser page title matches Paste Name / Title', async () => {

        const savedPasteTitle = 'how to gain dominance among developers - Pastebin.com'; 
        const currentTitle = await browser.getTitle();
        expect(currentTitle).toBe(savedPasteTitle);
    })

    it('Syntax is suspended for bash', async () => {
        const selectBox = $('[name = "PostForm[format]"]');
        selectBox.click();
        const selectBoxOption = $('//*[text()="Bash"]');
        selectBoxOption.waitForDisplayed();
        selectBoxOption.click();
    
        expect(selectBoxOption.toEqual('Bash')); 
    });

    it('Code matches the one entered', async () => {
        const expectedCode = `git config --global user.name 'New Sheriff in Town' git reset $(git commit-tree HEAD^{tree} -m 'Legacy code') git push origin master --force`;
    
        const pasteContent = `git config --global user.name 'New Sheriff in Town' git reset $(git commit-tree HEAD^{tree} -m 'Legacy code') git push origin master --force`;
    
        expect(pasteContent.trim()).toBe(expectedCode.trim());
    });

})

